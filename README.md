<h1>Forum API (based on Django REST framework)</h1>

<h2>Run and configure project:</h2>

Install requirements.txt
   
    run:
        python3 -m pip install -r requirements.txt

Migrations
   
    open rest_api_forum directory, run:
        cd rest_api_forum
    
    run:
        python3 manage.py makemigrations
        python3 manage.py migrate

create superuser
    
    run:
        python3 manage.py createsuperuser
    and enter needed credentials
**provide username "admin" for superuser!!!**
    
make start configuration
    insert your oauth2 keys and secrects if needed in file rest_api_forum/rest_api_forum/settings.py

    run:
        python3 manage.py start_setting
    This command need to configure authorization.
**Superuser with username admin must be crated!!!**
    
<h2>Useful tips</h2>
<h4>If you need to add new social network for authentication do following:</h4>

    open file rest_forum_api/authorization/management/commands/start_setting.py
    add social backend to "social_backends_list" variable
    and configure backend in rest_api_forum/rest_api_forum/settings.py
    run one more time:
        python3 manage.py start_setting
**Superuser with username admin must be crated!!!**

<h4>Documentation</h4>
redoc: 

    http://host:port/redoc/

swagger:

    http://host:port/swagger/
