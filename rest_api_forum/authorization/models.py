from django.contrib.auth.models import AbstractUser
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.dispatch import receiver
from django.utils import timezone
from django.db import models

from django_rest_passwordreset.signals import reset_password_token_created

from datetime import timedelta


class User(AbstractUser):
    profile_photo = models.ForeignKey('Photo', on_delete=models.SET_NULL, blank=True, null=True)
    notifications = models.ManyToManyField('forum.Message', related_name='notifications', blank=True)
    muted_for = models.DateTimeField(null=True, blank=True)
    banned = models.BooleanField(default=False)

    def delete(self, *args, **kwargs):
        """
        overwrite delete method for deleting related messages
        """
        self.notifications.all().delete()
        super().delete(*args, **kwargs)

    def mute(self, minutes):
        if self.muted_for:
            self.muted_for += timedelta(minutes=minutes)
        else:
            self.muted_for = timezone.now() + timedelta(minutes=minutes)

        self.save()

    def ban(self):
        messages = self.messages.all()
        for message in messages:
            message.content = None
        self.banned = True
        self.save()


class Photo(models.Model):
    photo = models.ImageField(upload_to='photoes/')

    def __str__(self):
        return self.photo.name


@receiver(reset_password_token_created)
def send_password_reset_mail(sender, instance, reset_password_token, *args, **kwargs):
    context = {
        'username': reset_password_token.user.username,
        'token': reset_password_token.key
    }
    subject = render_to_string('email/reset_password_letter_subject.txt', context).replace('\n', '')
    body = render_to_string('email/reset_password_letter_body.txt', context)
    send_mail(subject=subject, message=body, from_email='Django REST forum', recipient_list=['dpopoilyk@gmail.com', ])


