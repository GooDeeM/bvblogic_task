from django.core.management.base import BaseCommand
from django.db import connection

from oauth2_provider.models import Application
from authorization.models import User

class Command(BaseCommand):
    help = 'create applications for authorization (run only if superuser with username "admin" created)'

    def handle(self, *args, **options):
        """
        create applications for authorization
        """
        print('\nStart creating auth apps:\n')
        try:
            user = User.objects.get(username='admin')
            if not user.is_superuser:
                print('User with username "admin" is not superuser.')
                return
        except User.DoesNotExist:
            print('Superuser with username "admin" not found.')
            return

        print('Creating app for basic auth...', end=' ')
        app, created = Application.objects.get_or_create(
            name='base auth',
            user=user,
            client_type='confidential',
            authorization_grant_type='password'
        )
        if created:
            print('Success.')
        else:
            print('Already exists.')

        social_backends_list = ['vk-oauth2', ]  # add new backends there
        for sb in social_backends_list:
            print(f'Creating app for {sb}...', end=' ')
            app, created = Application.objects.get_or_create(
                name=sb,
                user=user,
                client_type='public',
                authorization_grant_type='client-credentials'
            )

            if created:
                print('Success.')
            else:
                print('Already exists.')

        print('\nStart setting complete.\n')
