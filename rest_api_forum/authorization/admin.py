from django.contrib import admin
from .models import User, Photo

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name']

@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    pass
