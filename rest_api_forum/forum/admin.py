from django.contrib import admin

from .models import Rubric, SubRubric, Topic, Message


@admin.register(Rubric)
class RubricAdmin(admin.ModelAdmin):
    list_display = ['name']

@admin.register(SubRubric)
class SubRubricAdmin(admin.ModelAdmin):
    list_display = ['name', 'rubric']

@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ['name', 'sub_rubric', 'creator']

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'author', 'timestamp']