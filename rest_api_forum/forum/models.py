from django.db import models, IntegrityError


class Rubric(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class SubRubric(models.Model):
    name = models.CharField(max_length=255)
    rubric = models.ForeignKey('Rubric', on_delete=models.CASCADE, related_name='sub_rubrics')

    def __str__(self):
        return self.name


class Topic(models.Model):
    name = models.CharField(max_length=255)
    sub_rubric = models.ForeignKey('SubRubric', on_delete=models.CASCADE, related_name='topics')
    creator = models.ForeignKey('authorization.User', on_delete=models.SET_NULL, related_name='creator_in', null=True)
    comments = models.ManyToManyField('Message', related_name='topics', blank=True)
    moderators = models.ManyToManyField('authorization.User', blank=True, related_name='moderator_in')
    open = models.BooleanField(default=True)

    def delete(self, *args, **kwargs):
        """
        overwrite delete method for deleting related messages
        """
        self.comments.all().delete()
        super().delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if not self.pk:
            duplicates_name = Topic.objects.filter(name=self.name)
            if duplicates_name:
                for duplicate in duplicates_name:
                    if duplicate.sub_rubric == self.sub_rubric:
                        raise IntegrityError('Topic with the same name is already exists in this sub rubric.')
        super(Topic, self).save(*args, **kwargs)

    def can_edit(self, user):
        """
        returns true if user can delete or edit topic
        """
        if user.is_superuser or user == self.creator or user in self.moderators.all():
            return True
        else:
            return False

    def __str__(self):
        return f'{self.name} (SubRubric: {self.sub_rubric})'


class Message(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name='was written')
    author = models.ForeignKey('authorization.User', on_delete=models.CASCADE, related_name='messages', null=True)
    content = models.TextField(null=True)
    reply_to = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    likes = models.PositiveIntegerField(default=0)

    # for messages
    edited = models.BooleanField(default=False)

    # for notifications
    checked = models.BooleanField(default=False)


    def __str__(self):
        if self.content:
            return f'{self.content[:50]}...' if len(self.content) > 50 else self.content
        else:
            return 'Deleted message.'

    def can_delete(self, user):
        """
        returns true if user can delete or edit message
        """
        topic = self.topics.first()
        if user.is_superuser or user == self.author or user in topic.moderators.all():
            return True
        else:
            return False