from django.utils import timezone

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from rest_framework.views import APIView
from rest_framework import status

from drf_yasg.utils import swagger_auto_schema

from .serializers import ImageSerializer, ChangePasswordSerializer, EditAccountSerializer, NotificationsListSerializer

from api.utils import cut_messages_list
from . import response_schemas


class UpdatePhotoView(APIView):
    parser_class = (FileUploadParser, )
    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(request_body=ImageSerializer, responses=response_schemas.ADD_PHOTO_SERIALIZER)
    def post(self, request, *args, **kwargs):
        """
        Update profile photo
        ---
        Need authentication!
        Only images in file parameter.
        """

        serializer = ImageSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            photo = serializer.save(request.user)
            data['photo'] = photo.photo.name
            data['user'] = request.user.username
            return Response(data, status=status.HTTP_201_CREATED)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordView(APIView):

    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(request_body=ChangePasswordSerializer, responses=response_schemas.CHANGE_PASSWORD_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        change user password
        ---
        if old_password is invalid returns error.
        if new_password the same as in database return error.
        """
        serializer = ChangePasswordSerializer(request.user, data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            data = {
                'response': 'Password successfully changed.',
                'user': user.username
            }
            return Response(data, status=status.HTTP_200_OK)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EditAccountView(APIView):

    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(request_body=EditAccountSerializer, responses=response_schemas.EDIT_ACCOUNT_RESPONSE)
    def post(self, request, *args, **kwargs):
        """
        Edit user account.
        ---
        if you don't wanna change field, don't give it.
        If field the same as in database, endpoint will return success but add warnings.
        """

        serializer = EditAccountSerializer(instance=request.user, data=request.data)
        if serializer.is_valid():
            data = {}

            user, warnings = serializer.save()
            data['response'] = 'Account data changed.'
            data['email'] = user.email
            data['username'] = user.username
            data['first_name'] = user.first_name
            data['last_name'] = user.last_name
            if warnings:
                data['warnings'] = warnings
            return Response(data, status=status.HTTP_201_CREATED)

        else:
            return Response(serializer.errors)


class NotificationsListView(APIView):

    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(query_serializer=NotificationsListSerializer(),
                         responses=response_schemas.ACCOUNT_NOTIFICATIONS_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        returns list of notifications
        ---
        If only_new parameter is false or not provided after call all returned notifications will be marked as checked.
        """
        serializer = NotificationsListSerializer(data=request.query_params)
        if serializer.is_valid():
            only_new = False
            if 'only_new' in serializer.validated_data.keys():
                only_new = serializer.validated_data['only_new']

            if only_new:
                notifications = request.user.notifications.filter(checked=False)
            else:
                notifications = request.user.notifications.all()

            # getting start value if given (will give results not including)
            offset = serializer.validated_data['offset'] if 'offset' in serializer.validated_data.keys() else None

            # getting limit value if given
            limit = serializer.validated_data['limit'] if 'limit' in serializer.validated_data.keys() else None

            # cutting notifications list
            notifications = cut_messages_list(notifications, offset=offset, limit=limit)

            if notifications:
                described_notifications = []

                # generating comments list
                for notification in notifications:
                    described_notification = {
                        'notification_id': notification.id,
                        'content': notification.content,
                        'checked': notification.checked
                    }

                    described_notifications.append(described_notification)

                # mark all notifications as checked if only_new is false
                if not only_new:
                    for notification in notifications:
                        if not notification.checked:
                            notification.checked = True
                            notification.save()

                # preparing data
                data = {
                    'user': request.user.username,
                    'user_id': request.user.id,
                    'count': len(described_notifications),
                    'Notifications': described_notifications
                }

                return Response(data=data, status=status.HTTP_200_OK)

            else:
                return Response(data={'error': 'Notifications not found.'}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(data=serializer.errors, status=status.HTTP_404_NOT_FOUND)


class NewNotificationsCountView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses=response_schemas.ACCOUNT_NOTIFICATIONS_COUNT_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        returns count of unchecked notifications.
        """
        count = request.user.notifications.filter(checked=False).count()
        return Response(data={'notifications_count': count})


class AccountInfoView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses=response_schemas.ACCOUNT_INFO_RESPONSE)
    def get(self, request, *args, **kwargs):
        topics_moder = []
        for topic in request.user.moderator_in.all():
            topics_moder.append(
                {
                    'topic_id': topic.id,
                    'topic_name': topic.name,
                }
            )

        topics_creator = []
        for topic in request.user.creator_in.all():
            topics_creator.append(
                {
                    'topic_id': topic.id,
                    'topic_name': topic.name,
                }
            )

        # unmute user if its time
        if request.user.muted_for and timezone.now() >= request.user.muted_for:
            request.user.muted_for = None
            request.user.save()

        data = {
            'id': request.user.id,
            'username': request.user.username,
            'email': request.user.email,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'muted': False if not request.user.muted_for else request.user.muted_for,
            'banned': request.user.banned,
            'moderator_in': topics_moder,
            'creator_in': topics_creator,
            'superuser': request.user.is_superuser
        }

        return Response(data=data, status=status.HTTP_200_OK)