from django.core.validators import MinValueValidator

from rest_framework import serializers

from authorization.models import Photo, User
from api.utils import send_register_activation


class ImageSerializer(serializers.Serializer):
    photo = serializers.ImageField(required=True)

    class Meta:
        fields = ['photo']

    def save(self, user):
        photo = Photo.objects.create(photo=self.validated_data['photo'])
        if user.profile_photo:
            user.profile_photo.delete()

        user.profile_photo = photo
        user.save()

        return photo


class ChangePasswordSerializer(serializers.ModelSerializer):

    old_password = serializers.CharField(required=True, style={'input_type': 'password'}, write_only=True)
    new_password = serializers.CharField(required=True, style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = User
        fields = ['old_password', 'new_password']

    def save(self):
        if not self.instance.check_password(self.validated_data['old_password']):
            raise serializers.ValidationError({'old_password': 'Wrong password.'})

        if not self.instance.check_password(self.validated_data['old_password']):
            raise serializers.ValidationError({'new_password': 'The same as old password.'})

        self.instance.set_password(self.validated_data['new_password'])
        self.instance.save()

        return self.instance


class EditAccountSerializer(serializers.ModelSerializer):

    host = serializers.URLField(required=False,
                                help_text='host for activation link generation. Provide it if you change email.')
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'host']

        extra_kwargs = {
            'first_name': {'required': False},
            'last_name': {'required': False},
            'username': {'required': False},
            'email': {'required': False},
            'host': {'required': False}
        }

    def save(self):
        warnings = {}

        if 'first_name' in self.validated_data.keys():
            if self.instance.first_name != self.validated_data['first_name']:
                self.instance.first_name = self.validated_data['first_name']
            else:
                warnings.update({'first_name': 'The same as in db.'})

        if 'last_name' in self.validated_data.keys():
            if self.instance.last_name != self.validated_data['last_name']:
                self.instance.last_name = self.validated_data['last_name']
            else:
                warnings.update({'last_name': 'The same as in db.'})

        if 'username' in self.validated_data.keys():
            if self.instance.username != self.validated_data['username']:
                self.instance.username = self.validated_data['username']
            else:
                warnings.update({'username': 'The same as in db.'})


        # if user change email, send activation message and make user unactivated
        if 'email' in self.validated_data.keys():
            if self.instance.email != self.validated_data['email']:
                if 'host' not in self.validated_data.keys():
                    raise serializers.ValidationError(
                        {'email': 'host must be provided with email for sending confirm message.'}
                    )
                self.instance.email = self.validated_data['email']
                self.instance.is_active = False

                # send activation email
                send_register_activation(self.instance.username,
                                         self.instance.email,
                                         self.validated_data['host']
                                         )
            else:
                warnings.update({'email': 'The same as in db.'})

        self.instance.save()

        return self.instance, warnings


class NotificationsListSerializer(serializers.Serializer):
    offset = serializers.IntegerField(required=False, validators=[MinValueValidator(0)])
    limit = serializers.IntegerField(required=False, validators=[MinValueValidator(1)])
    only_new = serializers.BooleanField(required=False)

    class Meta:
        fields = ['topic_id', 'offset', 'limit', 'only_new']
