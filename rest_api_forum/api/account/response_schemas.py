from drf_yasg import openapi


EDIT_ACCOUNT_RESPONSE = {
    200: openapi.Response(
        description='Successful edit.',
        schema=openapi.Schema(
            title='User',
            type=openapi.TYPE_OBJECT,
            properties={
                'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                'email': openapi.Schema(title='Email', type=openapi.TYPE_STRING, format=openapi.FORMAT_EMAIL),
                'username': openapi.Schema(title='Username', type=openapi.TYPE_STRING),
                'first_name': openapi.Schema(title='First name', type=openapi.TYPE_STRING),
                'last_name': openapi.Schema(title='Last name', type=openapi.TYPE_STRING),
                'warnings': openapi.Schema(title='Warnings',
                                           type=openapi.TYPE_OBJECT,
                                           properties={
                                                   '<parameter>': openapi.Schema(
                                                       title='Warning parameter',
                                                       type=openapi.TYPE_STRING,
                                                       default='The same as in db.'
                                                   )
                                               }
                                           )
            }
        )
    )
}

topics_array = openapi.Schema(
    title='', type=openapi.TYPE_ARRAY,
    items=openapi.Schema(title='', type=openapi.TYPE_OBJECT,
                         properties={
                            'topic_id': openapi.Schema(title='Topic id', type=openapi.TYPE_INTEGER),
                            'topic_name': openapi.Schema(title='Topic name', type=openapi.TYPE_STRING)
                            }
                         )
    )

ACCOUNT_INFO_RESPONSE = {
    200: openapi.Response(
        description='User data.',
        schema=openapi.Schema(
            title='User',
            type=openapi.TYPE_OBJECT,
            properties={
                'id': openapi.Schema(title='Id', type=openapi.TYPE_INTEGER),
                'email': openapi.Schema(title='Email', type=openapi.TYPE_STRING, format=openapi.FORMAT_EMAIL),
                'username': openapi.Schema(title='Username', type=openapi.TYPE_STRING),
                'first_name': openapi.Schema(title='First name', type=openapi.TYPE_STRING),
                'last_name': openapi.Schema(title='Last name', type=openapi.TYPE_STRING),
                'muted': openapi.Schema(title='Is user muted', type=openapi.TYPE_BOOLEAN,
                                        description='if user is muted return unmute time.'),
                'banned': openapi.Schema(title='Is user banned', type=openapi.TYPE_BOOLEAN),
                'moderator_in': topics_array,
                'creator_in': topics_array
            }
        )
    )
}

notifications_array = openapi.Schema(
    title='Notifications', type=openapi.TYPE_ARRAY,
    items=openapi.Schema(title='', type=openapi.TYPE_OBJECT,
                         properties={
                            'notification_id': openapi.Schema(title='Notification id', type=openapi.TYPE_INTEGER),
                            'content': openapi.Schema(title='Notification content', type=openapi.TYPE_STRING),
                            'checked': openapi.Schema(title='Checked', type=openapi.TYPE_BOOLEAN)
                            }
                         )
    )

ACCOUNT_NOTIFICATIONS_RESPONSES = {
    200: openapi.Response(
        description='User notifications.',
        schema=openapi.Schema(
            title='User',
            type=openapi.TYPE_OBJECT,
            properties={
                'user': openapi.Schema(title='Username', type=openapi.TYPE_STRING),
                'id': openapi.Schema(title='Id', type=openapi.TYPE_INTEGER),
                'count': openapi.Schema(title='Count of notifications', type=openapi.TYPE_INTEGER),
                'Notifications': notifications_array
            }
        )
    )
}

ACCOUNT_NOTIFICATIONS_COUNT_RESPONSES = {
    200: openapi.Response(
        description='Count of user notifications.',
        schema=openapi.Schema(
                    title='Count',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'notifications_count': openapi.Schema(title='Count of notifications', type=openapi.TYPE_INTEGER)
                    }
        )
    )
}

CHANGE_PASSWORD_RESPONSES = {
    200: openapi.Response(
        description='Password changed.',
        schema=openapi.Schema(
                    title='Count',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING,
                                                   description='Password successfully changed.'),
                        'user': openapi.Schema(title='Username', type=openapi.TYPE_STRING)
                    }
        )
    )
}

ADD_PHOTO_SERIALIZER = {
    200: openapi.Response(
        description='Photo updated',
        schema=openapi.Schema(
            title='Photo',
            type=openapi.TYPE_OBJECT,
            properties={
                'photo': openapi.Schema(title='photo', type=openapi.TYPE_STRING),
                'user': openapi.Schema(title='Username', type=openapi.TYPE_STRING)
            }
        )
    )
}