from django.urls import path, include

from .views import (
    UpdatePhotoView, ChangePasswordView, EditAccountView, NotificationsListView, NewNotificationsCountView,
    AccountInfoView
)

from django_rest_passwordreset import urls

urlpatterns = [
    path('photo/update/', UpdatePhotoView.as_view(), name='update_account_photo'),
    path('password/change/', ChangePasswordView.as_view(), name='change_password'),
    path('password/reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('edit/', EditAccountView.as_view(), name='edit_account'),
    path('notifications/', NotificationsListView.as_view(), name='notifications_list'),
    path('notifications/new/count/', NewNotificationsCountView.as_view(), name='notifications_count'),
    path('info/', AccountInfoView.as_view(), name='account_info')
]
