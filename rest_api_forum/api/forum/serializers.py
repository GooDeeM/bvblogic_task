from django.utils import timezone
from django.core.validators import MinValueValidator

from rest_framework import serializers
from rest_framework.exceptions import NotFound

from forum.models import Topic, SubRubric, Message
from authorization.models import User


class CreateTopicSerializer(serializers.ModelSerializer):
    sub_rubric_id = serializers.IntegerField(required=True)

    class Meta:
        model = Topic
        fields = ['name', 'sub_rubric_id']

        extra_kwargs = {
            'name': {'required': True},
        }

    def save(self, user):
        if user.muted_for:
            if user.muted_for <= timezone.now():
                user.muted_for = None
                user.save()
            else:
                raise serializers.ValidationError({'error': 'User muted.', 'muted_for': user.muted_for})

        try:
            sub_rubric = SubRubric.objects.get(id=self.validated_data['sub_rubric_id'])
        except SubRubric.DoesNotExist:
            raise NotFound(detail='Sub rubric not found.')

        topics = sub_rubric.topics.filter(name=self.validated_data['name'])
        if topics:
            raise serializers.ValidationError({'name': 'Topic with this name already exists in this sub rubric'})

        topic = Topic.objects.create(
            name=self.validated_data['name'],
            sub_rubric=sub_rubric,
            creator=user
        )

        return topic


class DeleteTopicSerializer(serializers.Serializer):
    topic_id = serializers.IntegerField(required=True)
    reason = serializers.CharField(required=False)

    class Meta:
        fields = ['topic_id', 'reason']

    def save(self, user):
        try:
            topic = Topic.objects.get(id=self.validated_data['topic_id'])
        except Topic.DoesNotExist:
            raise NotFound('Topic not found.')

        if topic.can_edit(user):
            topic.delete()
        else:
            raise serializers.ValidationError({'error': 'User have not permissions to delete this topic'})

        # send notification to topic creator if topic has been deleted by moderator or admin
        notification = None
        if 'reason' in self.validated_data.keys() and user != topic.creator:
            notification = Message.objects.create(
                timestamp=timezone.now(),
                author=user,
                content=f'The topic "{topic.name}" was deleted by {user}.\n\n'
                        f'reason: {self.validated_data["reason"]}'
            )
            topic.creator.notifications.add(notification)

        return topic, notification


class AddCommentSerializer(serializers.ModelSerializer):
    reply_message_id = serializers.IntegerField(required=False)
    topic_id = serializers.IntegerField(required=True)

    class Meta:
        model = Message

        fields = ['content', 'reply_message_id', 'topic_id']
        extra_kwargs = {
            'content': {'required': True}
        }

    def save(self, user):
        try:
            topic = Topic.objects.get(id=self.validated_data['topic_id'])
        except Topic.DoesNotExist:
            raise NotFound(detail='Topic not found')

        if user.muted_for:
            if user.muted_for <= timezone.now():
                user.muted_for = None
                user.save()
            else:
                raise serializers.ValidationError({'error': 'User muted.', 'muted_for': user.muted_for})

        comment = Message(
            timestamp=timezone.now(),
            author=user,
            content=self.validated_data['content'],
        )

        # add reply message if given
        notification = None
        if 'reply_message_id' in self.validated_data.keys():
            try:
                reply_comment = Message.objects.get(id=self.validated_data['reply_message_id'])
            except Message.DoesNotExist:
                raise NotFound(detail='Reply_to message not found.')
            comment.reply_to = reply_comment

            # send notification about reply
            notification = Message.objects.create(
                timestamp=timezone.now(),
                author=user,
                content=f'You have new reply in topic {topic.name}'
            )

            reply_comment.author.notifications.add(notification)

        comment.save()
        topic.comments.add(comment)

        return comment, topic, notification


class EditCommentSerializer(serializers.ModelSerializer):
    comment_id = serializers.IntegerField(required=True)
    class Meta:
        model = Message
        fields = ['comment_id', 'content']

        extra_kwargs = {
            'content': {'required': True}
        }

    def save(self, user):
        try:
            comment = Message.objects.get(id=self.validated_data['comment_id'])
        except Message.DoesNotExist:
            raise NotFound(detail='Comment not found.')

        if user != comment.author:
            raise serializers.ValidationError({'error': 'Only comment author can edit comment.'})

        comment.content = self.validated_data['content']
        comment.edited = True

        comment.save()

        return comment


class DeleteCommentSerializer(serializers.Serializer):
    comment_id = serializers.IntegerField(required=True)

    class Meta:
        fields = ['comment_id']

    def save(self, user):
        try:
            comment = Message.objects.get(id=self.validated_data['comment_id'])
        except Message.DoesNotExist:
            raise NotFound(detail='Comment not found.')

        if not comment.content:
            raise serializers.ValidationError({'error': 'Comment is already deleted.'})

        topic = comment.topics.first()
        if not topic:
            raise NotFound(detail='Comment topic not found.')

        if not comment.can_delete(user):
            raise serializers.ValidationError({'error': 'User have not permissions to delete this topic'})

        # send notification about deleting comment
        notification = None
        if comment.author != user:
            notification = Message.objects.create(
                timestamp=timezone.now(),
                author=user,
                content=f'Your comment in topic "{topic.name}" has been deleted by {user}.'
            )

            comment.author.notifications.add(notification)

        comment.content = None
        comment.save()

        return comment, notification


class CommentsListSerializer(serializers.Serializer):
    topic_id = serializers.IntegerField(required=True)
    offset = serializers.IntegerField(required=False, validators=[MinValueValidator(0)])
    limit = serializers.IntegerField(required=False, validators=[MinValueValidator(1)])

    class Meta:
        fields = ['topic_id', 'offset', 'limit']


class TopicSearchSerializer(serializers.Serializer):
    keyword = serializers.CharField(required=True)
    rubric_id = serializers.IntegerField(required=False)
    sub_rubric_id = serializers.IntegerField(required=False)

    class Meta:
        fields = ['keyword', 'rubric_id', 'sub_rubric_id']


class TopicNewModeratorSerializer(serializers.Serializer):
    topic_id = serializers.IntegerField(required=True)
    user_id = serializers.IntegerField(required=True)

    class Meta:
        fields = ['user_id']

    def save(self):
        try:
            topic = Topic.objects.get(id=self.validated_data['topic_id'])
        except Topic.DoesNotExist:
            raise NotFound(detail='Topic not found.')

        try:
            user = User.objects.get(id=self.validated_data['user_id'])
        except User.DoesNotExist:
            raise NotFound(detail='User not found.')

        topic.moderators.add(user)

        # send notification about promotion
        notification = Message.objects.create(
            timestamp=timezone.now(),
            author=user,
            content=f'You has been promoted to moderator in topic "{topic.name}" by administrator.'
        )
        user.notifications.add(notification)

        return topic, user


class CloseTopicSerializer(serializers.Serializer):
    topic_id = serializers.IntegerField(required=True)
    reason = serializers.CharField(required=True)

    class Meta:
        fields = ['topic_id', 'reason']

    def save(self, user):
        try:
            topic = Topic.objects.get(id=self.validated_data['topic_id'])
        except Topic.DoesNotExist:
            raise NotFound(detail='Topic not found.')

        if topic.can_edit(user):
            topic.open = False
        else:
            raise serializers.ValidationError({'error': 'User have not permissions to close this topic'})

            # send notification to topic creator if topic has been deleted by moderator or admin
        notification = None
        if 'reason' in self.validated_data.keys() and user != topic.creator:
            notification = Message.objects.create(
                timestamp=timezone.now(),
                author=user,
                content=f'The topic "{topic.name}" was closed by {user}.\n\n'
                        f'reason: {self.validated_data["reason"]}'
            )
            topic.creator.notifications.add(notification)

        topic.save()

        return topic, notification


class MuteUserSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=True)
    topic_id = serializers.IntegerField(required=True)
    reason = serializers.CharField(required=False)
    minutes = serializers.IntegerField(required=True, help_text='mute time (in minutes)')

    class Meta:
        fields = ['user_id', 'topic_id', 'reason']

    def save(self, user):
        try:
            topic = Topic.objects.get(id=self.validated_data['topic_id'])
        except Topic.DoesNotExist:
            raise NotFound(detail='Topic not found.')

        if user not in topic.moderators.all() and not user.is_superuser:
            raise serializers.ValidationError({'error': 'Only topic moderators and admins can mute user.'})

        try:
            user_to_mute = User.objects.get(id=self.validated_data['user_id'])
        except User.DoesNotExist:
            raise NotFound(detail='User to mute not found.')

        if user_to_mute == user:
            raise serializers.ValidationError({'error': "Can't mute yourself."})
        if user_to_mute.is_superuser:
            raise serializers.ValidationError({'error': "Can't mute admin."})

        user_to_mute.mute(minutes=self.validated_data['minutes'])

        notification_content = f'You has been muted to {user.muted_for} by {user.username}.'
        if 'reason' in self.validated_data.keys():
            notification_content += f'\n\nreason: {self.validated_data["reason"]}'

        # send notification
        notification = Message.objects.create(
            timestamp=timezone.now(),
            author=user,
            content=notification_content
        )
        user_to_mute.notifications.add(notification)

        return user_to_mute


class BanUserSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=True)
    topic_id = serializers.IntegerField(required=True)
    reason = serializers.CharField(required=False)

    class Meta:
        fields = ['user_id', 'topic_id', 'reason']

    def save(self, user):
        try:
            topic = Topic.objects.get(id=self.validated_data['topic_id'])
        except Topic.DoesNotExist:
            raise NotFound(detail='Topic not found.')

        if user not in topic.moderators.all() and not user.is_superuser:
            raise serializers.ValidationError({'error': 'Only topic moderators and admins can ban user.'})

        try:
            user_to_ban = User.objects.get(id=self.validated_data['user_id'])
        except User.DoesNotExist:
            raise NotFound(detail='User to ban not found.')

        if user_to_ban.banned:
            raise serializers.ValidationError({'error': "User is already banned."})

        if user_to_ban == user:
            raise serializers.ValidationError({'error': "Can't ban yourself."})

        if user_to_ban.is_superuser:
            raise serializers.ValidationError({'error': "Can't ban admin."})

        if not user.is_superuser and user in topic.moderators.all() and user_to_ban in topic.moderators.all():
            raise serializers.ValidationError({'error': "Moderator can't ban other moderators."})

        user_to_ban.ban()

        notification_content = f'You has been banned by {user.username}.'
        if 'reason' in self.validated_data.keys():
            notification_content += f'\n\nreason: {self.validated_data["reason"]}'

        # send notification
        notification = Message.objects.create(
            timestamp=timezone.now(),
            author=user,
            content=notification_content
        )
        user_to_ban.notifications.add(notification)

        return user_to_ban
