from django.urls import path

from .views import (
    RubricsListView, SubRubricsListView, CreateTopicView, CanDeleteView, DeleteTopicView, AddCommentView,
    EditCommentView, DeleteCommentView, CommentsListView, TopicSearchView, TopicNewModeratorView,
    CloseTopicView, SubRubricTopicsListView, MuteUserView, BanUserView
)

urlpatterns = [
    path('rubrics/list/', RubricsListView.as_view(), name='rubrics_list'),
    path('rubric/<int:rubric_id>/subrubrics/list/', SubRubricsListView.as_view(), name='subrubrics_list'),
    path('subrubric/<int:sub_rubric_id>/topics/list/', SubRubricTopicsListView.as_view(), name='subrubric_topics_list'),
    path('topic/create/', CreateTopicView.as_view(), name='create_topic'),
    path('topic/can-delete/<int:topic_id>/', CanDeleteView.as_view(), name='can_delete_topic'),
    path('topic/delete/', DeleteTopicView.as_view(), name='delete_topic'),
    path('topic/close/', CloseTopicView.as_view(), name='close_topic'),
    path('topic/comments-list/', CommentsListView.as_view(), name='comments_list'),
    path('topic/search/', TopicSearchView.as_view(), name='topics_search'),
    path('topic/new-moderator/', TopicNewModeratorView.as_view(), name='topics_new_moderator'),
    path('comment/add/', AddCommentView.as_view(), name='add_comment'),
    path('comment/edit/', EditCommentView.as_view(), name='edit_comment'),
    path('comment/delete/', DeleteCommentView.as_view(), name='delete_comment'),
    path('user/mute/', MuteUserView.as_view(), name='mute_user'),
    path('user/ban/', BanUserView.as_view(), name='ban_user')
]
