from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import (
    CreateTopicSerializer, DeleteTopicSerializer, AddCommentSerializer, EditCommentSerializer,
    DeleteCommentSerializer, CommentsListSerializer, TopicSearchSerializer, TopicNewModeratorSerializer,
    CloseTopicSerializer, MuteUserSerializer, BanUserSerializer
)

from forum.models import Rubric, Topic, SubRubric
from api.utils import cut_messages_list
from . import response_schemas

from drf_yasg.utils import swagger_auto_schema


class RubricsListView(APIView):

    @swagger_auto_schema(responses=response_schemas.RUBRICS_LIST_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        returns list of rubrics with its ids
        """

        rubrics = [{'name': rubric.name, 'id': rubric.id} for rubric in Rubric.objects.all()]
        return Response(rubrics, status=status.HTTP_200_OK) if rubrics else Response(status=status.HTTP_404_NOT_FOUND)


class SubRubricsListView(APIView):

    @swagger_auto_schema(responses=response_schemas.SUBRUBRICS_LIST_RESPONSES)
    def get(self, request, rubric_id):
        """
        returns list of subrubrics for given rubric
        """

        try:
            rubric = Rubric.objects.get(id=rubric_id)
        except Rubric.DoesNotExist:
            raise NotFound(detail='Rubric not found.')

        data = {}
        subrubrics = [{'name': sub_rubrics.name, 'id': sub_rubrics.id} for sub_rubrics in rubric.sub_rubrics.all()]

        data['rubric'] = rubric.name
        data['subrubrics'] = subrubrics

        return Response(data, status=status.HTTP_200_OK) if subrubrics else Response(status=status.HTTP_404_NOT_FOUND)


class CreateTopicView(APIView):

    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(query_serializer=CreateTopicSerializer(), responses=response_schemas.CREATE_TOPIC_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        Create topic
        ---
        Muted user cant create topic.
        """
        serializer = CreateTopicSerializer(data=request.query_params)

        if serializer.is_valid():
            topic = serializer.save(request.user)
            data = {
                'response': 'Successfully created.',
                'name': topic.name,
                'id': topic.id,
                'creator': topic.creator.username,
                'rubric': topic.sub_rubric.rubric.name,
                'sub_rubric': topic.sub_rubric.name
            }

            return Response(data=data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CanDeleteView(APIView):

    def get(self, request, topic_id, *args, **kwargs):
        """
        simple test API that returns true if user can delete topic
        """
        if Topic.objects.get(pk=topic_id).can_edit(request.user):
            return Response({'response': True})
        else:
            return Response({'response': False})


class DeleteTopicView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=DeleteTopicSerializer, responses=response_schemas.DELETE_TOPIC_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        Deleting topic.
        ---
        Only topic creator, topic moderators and admins can do that.
        if moderator or admin delete topic, to creator will be send notification about deleting.
        """

        serializer = DeleteTopicSerializer(data=request.data)
        if serializer.is_valid():
            topic, notification = serializer.save(user=request.user)
            data = {
                'response': 'Topic successfully deleted.',
                'topic': topic.name,
                'topic_id': topic.id
            }
            if notification:
                data['response'] += ' Notification to creator sent.'

            return Response(data=data, status=status.HTTP_200_OK)
        else:
            return Response(data=serializer.errors)


class AddCommentView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=AddCommentSerializer, responses=response_schemas.CREATE_COMMENT_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        Add comment.
        ---
        Only for authenticated and not muted users.
        If comment is reply to other comment, notification will be sent.
        Muted user cant add comment.
        """
        serializer = AddCommentSerializer(data=request.data)
        if serializer.is_valid():
            comment, topic, notification = serializer.save(request.user)
            data = {
                'response': 'Comment successfully created.',
                'topic': topic.name,
                'topic_id': topic.id,
                'comment_id': comment.id
            }

            if notification:
                data['response'] += ' Notification about reply sent.'

            return Response(data=data, status=status.HTTP_201_CREATED)

        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EditCommentView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=EditCommentSerializer, responses=response_schemas.EDIT_COMMENT_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        edit comment.
        ---
        only comment author can edit comment.
        """
        serializer = EditCommentSerializer(data=request.data)
        if serializer.is_valid():
            comment = serializer.save(user=request.user)
            data = {
                'response': 'Comment successfully edited.',
                'comment_id': comment.id
            }

            return Response(data=data, status=status.HTTP_200_OK)

        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteCommentView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(query_serializer=DeleteCommentSerializer(),
                         responses=response_schemas.DELETE_COMMENT_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        delete comment.
        ---
        Only comment author, moderators or admins can delete comments
        """
        serializer = DeleteCommentSerializer(data=request.query_params)
        if serializer.is_valid():
            comment, notification = serializer.save(request.user)
            data = {
                'response': 'Comment successfully deleted.',
                'comment_id': comment.id
            }

            if notification:
                data['response'] += ' Notification about deleting sent.'

            return Response(data, status=status.HTTP_200_OK)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentsListView(APIView):

    @swagger_auto_schema(query_serializer=CommentsListSerializer(), responses=response_schemas.COMMENTS_LIST_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        List of comments in topic.
        ---
        can provide limit and offset for pagination.
        limit = maximum count of comments.
        offset = starting point, if you need to show comments, for example, from 10 (not including)
        """
        serializer = CommentsListSerializer(data=request.query_params)
        if serializer.is_valid():
            try:
                topic = Topic.objects.get(id=serializer.validated_data['topic_id'])
            except Topic.DoesNotExist:
                raise NotFound(detail='Topic not found.')
            comments = topic.comments.all()

            # getting start value if given (will give results not including)
            offset = serializer.validated_data['offset'] if 'offset' in serializer.validated_data.keys() else None

            # getting limit value if given
            limit = serializer.validated_data['limit'] if 'limit' in serializer.validated_data.keys() else None

            # cutting comments list
            comments = cut_messages_list(comments, limit=limit, offset=offset)

            if comments:
                described_comments = []

                # generating comments list
                for comment in comments:
                    described_comment = {
                        'comment_id': comment.id,
                        'author': comment.author.username,
                        'author_id': comment.author.id,
                        'content': comment.content,
                        'edited': comment.edited
                    }
                    if comment.reply_to:
                        described_comment['reply_to_comment_id'] = comment.reply_to.id

                    described_comments.append(described_comment)

                # preparing data
                data = {
                    'topic': topic.name,
                    'topic_id': topic.id,
                    'count': len(described_comments),
                    'comments': described_comments
                }

                return Response(data=data, status=status.HTTP_200_OK)

            else:
                return Response({'error': 'Comments not found.'}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TopicSearchView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(query_serializer=TopicSearchSerializer(), responses=response_schemas.TOPICS_SEARCH_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        Search topics by keyword.
        ---
        can provide rubric and sub rubric to search.
        if you provide sub rubric, rubric filter does not work (it makes not sense).
        """
        serializer = TopicSearchSerializer(data=request.query_params)

        if serializer.is_valid():
            topics = Topic.objects.filter(name__contains=serializer.validated_data['keyword'])
            if not topics:
                return Response(data={'response': 'Topics not found.'}, status=status.HTTP_404_NOT_FOUND)

            if 'sub_rubric_id' in serializer.validated_data.keys():
                try:
                    sub_rubric = SubRubric.objects.get(id=serializer.validated_data['sub_rubric_id'])
                except SubRubric.DoesNotExist:
                    raise NotFound(detail='Sub rubric not found.')

                topics = topics.filter(sub_rubric=sub_rubric)

            # if sub_rubric_id is provided rubric filter does not work
            elif 'rubric_id' in serializer.validated_data.keys():
                try:
                    rubric = Rubric.objects.get(id=serializer.validated_data['rubric_id'])
                except Rubric.DoesNotExist:
                    raise NotFound(detail='Rubric not found.')

                topics = topics.filter(sub_rubric__rubric=rubric)

            if topics:
                data = {
                    'count': len(topics),
                    'topics': [{'id': topic.id, 'name': topic.name} for topic in topics]
                }
                return Response(data=data, status=status.HTTP_200_OK)
            else:
                return Response(data={'response': 'Topics not found.'}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TopicNewModeratorView(APIView):
    permission_classes = [IsAuthenticated, IsAdminUser]

    @swagger_auto_schema(query_serializer=TopicNewModeratorSerializer(),
                         responses=response_schemas.NEW_MODERATOR_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        appoint a moderator (only for users that have admin status)
        """
        serializer = TopicNewModeratorSerializer(data=request.query_params)

        if serializer.is_valid():
            topic, user = serializer.save()
            data = {
                'response': 'Successfully added new moderator to topic.',
                'topic': topic.name,
                'topic_id': topic.id,
                'user': user.username,
                'user_id': user.id,
            }

            return Response(data=data, status=status.HTTP_201_CREATED)

        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CloseTopicView(APIView):

    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=CloseTopicSerializer, responses=response_schemas.CLOSE_TOPIC_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        close topic
        ---
        Only topic creator, moderators and admins can close topic.
        if moderator or admin close topic, notification will be sent to creator.
        """
        serializer = CloseTopicSerializer(data=request.data)

        if serializer.is_valid():
            topic, notification = serializer.save(user=request.user)
            data = {
                'response': 'Topic successfully closed.',
                'topic': topic.name,
                'topic_id': topic.id
            }
            if notification:
                data['response'] += ' Notification to creator sent.'

            return Response(data=data, status=status.HTTP_200_OK)

        else:
            return Response(data=serializer.errors)


class SubRubricTopicsListView(APIView):

    @swagger_auto_schema(responses=response_schemas.TOPICS_LIST_RESPONSES)
    def get(self, request, sub_rubric_id, *args, **kwargs):
        """
        returns list of topics in sub rubric with its names and ids
        """
        try:
            sub_rubric = SubRubric.objects.get(id=sub_rubric_id)
        except SubRubric.DoesNotExist:
            return Response({'response': 'Sub rubric not found'})

        topics = sub_rubric.topics.all()
        if topics:
            data = {
                'count': len(topics),
                'topics': [{'id': topic.id, 'name': topic.name} for topic in topics]
            }
            return Response(data=data, status=status.HTTP_200_OK)

        else:
            return Response({'response': 'topics not found'})


class MuteUserView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=MuteUserSerializer, responses=response_schemas.BAN_USER_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        mute user.
        ---
        Notification about mute will be sent to user.
        Only topic moderators and admins can mute users.
        Cant mute admin.
        """
        serializer = MuteUserSerializer(data=request.data)

        if serializer.is_valid():
            user = serializer.save(request.user)
            data = {
                'response': 'User muted. Notification sent.',
                'user': user.username,
                'user_id': user.id
            }

            return Response(data=data, status=status.HTTP_200_OK)

        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BanUserView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=BanUserSerializer(), responses=response_schemas.BAN_USER_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        ban user.
        ---
        Notification about ban will be sent to user.
        Only topic moderators and admins can mute users.
        Can't ban admin.
        """
        serializer = BanUserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save(request.user)
            data = {
                'response': 'User banned. Notification was send.',
                'user': user.username,
                'user_id': user.id
            }

            return Response(data=data, status=status.HTTP_200_OK)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
