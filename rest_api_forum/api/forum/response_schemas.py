from drf_yasg import openapi

CREATE_COMMENT_RESPONSES = {
    201: openapi.Response(
                description='Created',
                schema=openapi.Schema(
                    title='Comment',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                        'topic': openapi.Schema(title='Topic name', type=openapi.TYPE_STRING),
                        'topic_id': openapi.Schema(title='Topic id', type=openapi.TYPE_INTEGER),
                        'comment_id': openapi.Schema(title='Comment id', type=openapi.TYPE_INTEGER),
                    }
                )
            )

}

DELETE_COMMENT_RESPONSES = {
    200: openapi.Response(
        description='Deleted',
        schema=openapi.Schema(
            title='Comment',
            type=openapi.TYPE_OBJECT,
            properties={
                'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING,
                                           description='Comment successfully deleted.'),
                'comment_id': openapi.Schema(title='Comment id', type=openapi.TYPE_INTEGER),
                }
            )
        )
}

EDIT_COMMENT_RESPONSES = {
    200: openapi.Response(
        description='Deleted',
        schema=openapi.Schema(
            title='Comment',
            type=openapi.TYPE_OBJECT,
            properties={
                'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING,
                                           description='Comment successfully edited.'),
                'comment_id': openapi.Schema(title='Comment id', type=openapi.TYPE_INTEGER),
                }
            )
        )
}

subrubrics_array = openapi.Schema(
    title='', type=openapi.TYPE_ARRAY,
    items=openapi.Schema(title='', type=openapi.TYPE_OBJECT,
                         properties={
                            'name': openapi.Schema(title='Rubric name', type=openapi.TYPE_STRING),
                            'id': openapi.Schema(title='Topic name', type=openapi.TYPE_STRING)
                            }
                         )
    )

SUBRUBRICS_LIST_RESPONSES = {
    200: openapi.Response(
        description='List.',
        schema=openapi.Schema(
            title='Subrubrics',
            type=openapi.TYPE_OBJECT,
            properties={
                'rubric': openapi.Schema(title='Rubric name', type=openapi.TYPE_STRING),
                'subrubrics': subrubrics_array
                }
            )
        )
}

RUBRICS_LIST_RESPONSES = {
    200: openapi.Response(
        description='List.',
        schema=subrubrics_array
        )
}

TOPICS_LIST_RESPONSES = {
    200: openapi.Response(
        description='List.',
        schema=openapi.Schema(
            title='Topics',
            type=openapi.TYPE_OBJECT,
            properties={
                'count': openapi.Schema(title='Topics count', type=openapi.TYPE_INTEGER),
                'subrubrics': subrubrics_array
                }
            )
        )
}

CLOSE_TOPIC_RESPONSES = {
    200: openapi.Response(
        description='Closed.',
        schema=openapi.Schema(
            title='topic',
            type=openapi.TYPE_OBJECT,
            properties={
                'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                'topic': openapi.Schema(title='Topic', type=openapi.TYPE_STRING),
                'topic_id': openapi.Schema(title='Topic ID', type=openapi.TYPE_INTEGER),
                }
            )
        )
}

comments_array = openapi.Schema(
    title='', type=openapi.TYPE_ARRAY,
    items=openapi.Schema(title='', type=openapi.TYPE_OBJECT,
                         properties={
                            'comment_id': openapi.Schema(title='Comment ID', type=openapi.TYPE_INTEGER),
                            'author': openapi.Schema(title='Comment author', type=openapi.TYPE_STRING),
                            'author_id': openapi.Schema(title='Comment author ID', type=openapi.TYPE_INTEGER),
                            'content': openapi.Schema(title='Comment content', type=openapi.TYPE_STRING),
                            'edited': openapi.Schema(title='Was comment edited', type=openapi.TYPE_BOOLEAN),
                            'reply_comment_id': openapi.Schema(title='Reply to comment ID', type=openapi.TYPE_INTEGER),
                            }
                         )
    )

COMMENTS_LIST_RESPONSES = {
    200: openapi.Response(
        description='Closed.',
        schema=openapi.Schema(
            title='topic',
            type=openapi.TYPE_OBJECT,
            properties={
                'topic': openapi.Schema(title='Topic', type=openapi.TYPE_STRING),
                'topic_id': openapi.Schema(title='Topic ID', type=openapi.TYPE_INTEGER),
                'count': openapi.Schema(title='Count of comments', type=openapi.TYPE_INTEGER),
                'comments': comments_array
                }
            )
        )
}


CREATE_TOPIC_RESPONSES = {
    201: openapi.Response(
                description='Created',
                schema=openapi.Schema(
                    title='Comment',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING,
                                                   description='Successfully created'),
                        'name': openapi.Schema(title='Topic name', type=openapi.TYPE_STRING),
                        'id': openapi.Schema(title='Topic id', type=openapi.TYPE_INTEGER),
                        'creator': openapi.Schema(title='Creator username', type=openapi.TYPE_STRING),
                        'rubric': openapi.Schema(title='Rubric name', type=openapi.TYPE_STRING),
                        'sub_rubric': openapi.Schema(title='Sub Rubric name', type=openapi.TYPE_STRING),
                    }
                )
            )
}

DELETE_TOPIC_RESPONSES = {
    200: openapi.Response(
                description='Deleted',
                schema=openapi.Schema(
                    title='Comment',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                        'topic': openapi.Schema(title='Topic name', type=openapi.TYPE_STRING),
                        'topic_id': openapi.Schema(title='Topic id', type=openapi.TYPE_INTEGER),
                    }
                )
            )
}

NEW_MODERATOR_RESPONSES = {
    200: openapi.Response(
                description='Deleted',
                schema=openapi.Schema(
                    title='Comment',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                        'topic': openapi.Schema(title='Topic name', type=openapi.TYPE_STRING),
                        'topic_id': openapi.Schema(title='Topic id', type=openapi.TYPE_INTEGER),
                        'user': openapi.Schema(title='User username', type=openapi.TYPE_STRING),
                        'user_id': openapi.Schema(title='User id', type=openapi.TYPE_INTEGER),
                    }
                )
            )
}

TOPICS_SEARCH_RESPONSES = {
    200: openapi.Response(
        description='List.',
        schema=openapi.Schema(
            title='Topics',
            type=openapi.TYPE_OBJECT,
            properties={
                'count': openapi.Schema(title='Topics count', type=openapi.TYPE_INTEGER),
                'subrubrics': subrubrics_array
                }
            )
        )
}

BAN_USER_RESPONSES = {
    200: openapi.Response(
                description='Deleted',
                schema=openapi.Schema(
                    title='Comment',
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                        'user': openapi.Schema(title='User username', type=openapi.TYPE_STRING),
                        'user_id': openapi.Schema(title='User id', type=openapi.TYPE_INTEGER),
                    }
                )
            )
}
