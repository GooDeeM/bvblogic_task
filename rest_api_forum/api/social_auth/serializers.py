from rest_framework import serializers


class ConvertTokenSerializerSwagger(serializers.Serializer):
    grant_type = serializers.CharField(required=True)
    client_id = serializers.CharField(required=True)
    backend = serializers.CharField(required=True)
    token = serializers.CharField(required=True)

    class Meta:
        fields = ['grant_type', 'client_id', 'backend', 'token']