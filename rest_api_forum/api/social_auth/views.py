from rest_framework_social_oauth2.views import ConvertTokenView

from drf_yasg.utils import swagger_auto_schema

from .serializers import ConvertTokenSerializerSwagger
from . import response_schemas


class SwaggerConvertTokenView(ConvertTokenView):
    """
    Implements an endpoint to convert a provider token to an access token
    """
    @swagger_auto_schema(request_body=ConvertTokenSerializerSwagger, responses=response_schemas.CONVERT_TOKEN_RESPONSES)
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)