from drf_yasg import openapi

CONVERT_TOKEN_RESPONSES = {
    200: openapi.Response(
        description='Created',
        schema=openapi.Schema(
            title='Token',
            type=openapi.TYPE_OBJECT,
            properties={
                'access_token': openapi.Schema(title='Access token', type=openapi.TYPE_STRING),
                'expires_in': openapi.Schema(title='Expires in', type=openapi.TYPE_INTEGER),
                'token_type': openapi.Schema(title='Token type', type=openapi.TYPE_STRING),
                'scope': openapi.Schema(title='Scope', type=openapi.TYPE_STRING),
                'refresh_token': openapi.Schema(title='Refresh token', type=openapi.TYPE_STRING),
            }
        )
    )
}
