from django.conf.urls import url, include
from django.urls import path
from oauth2_provider.views import AuthorizationView

from .views import SwaggerConvertTokenView
from rest_framework_social_oauth2.views import invalidate_sessions

urlpatterns = [
    path('authorize/', AuthorizationView.as_view(), name="authorize"),
    path('', include('social_django.urls', namespace="social")),
    path('convert-token/', SwaggerConvertTokenView.as_view(), name="convert_token"),
    path('invalidate-sessions/', invalidate_sessions, name="invalidate_sessions")
]
