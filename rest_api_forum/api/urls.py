from django.urls import path, include

from .views import TestView

from rest_framework import routers
router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('test/', TestView.as_view(), name='api_test'),
    path('authorization/', include('api.authorization.urls'), name='authorization'),
    path('social-auth/', include('api.social_auth.urls'), name='social_authentication'),
    path('account/', include('api.account.urls'), name='account'),
    path('forum/', include('api.forum.urls'), name='forum')
]