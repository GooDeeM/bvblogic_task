from django.template.loader import render_to_string
from django.core.signing import Signer
from django.core.mail import send_mail
signer = Signer()


def send_register_activation(username, email, host):
    context = {
        'username': username,
        'host': host,
        'sign': signer.sign(username)
    }
    subject = render_to_string('email/activation_letter_subject.txt', context).replace('\n', '')
    body = render_to_string('email/activation_letter_body.txt', context)
    send_mail(subject=subject, message=body, from_email='Django REST forum', recipient_list=[email, ])


def cut_messages_list(comments, limit=None, offset=None):
    if offset and limit:
        if len(comments) > offset and len(comments) > limit + offset:
            comments = comments[offset:limit + offset]

        elif len(comments) <= limit + offset:
            comments = comments[offset:]

        elif len(comments) <= offset:
            comments = None

    elif limit:
        if len(comments) > limit:
            comments = comments[:limit]

    elif offset:
        if len(comments) >= offset:
            comments = comments[offset:]
        else:
            comments = None

    return comments