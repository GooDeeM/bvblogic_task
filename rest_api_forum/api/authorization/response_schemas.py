from drf_yasg import openapi

AUTH_APPS_LIST_RESPONSES = {
    200: openapi.Response(
        description='Apps list.',
        schema=openapi.Schema(
            title='Apps',
            type=openapi.TYPE_ARRAY,
            items=openapi.Schema(
                title='App',
                type=openapi.TYPE_OBJECT,
                properties={
                    'name_backend': openapi.Schema(title='Backend', type=openapi.TYPE_STRING),
                    'client_id': openapi.Schema(title='Client ID', type=openapi.TYPE_STRING),
                    'client_secret': openapi.Schema(title='Cient secret', type=openapi.TYPE_INTEGER),
                }
            )
        )
    )
}

REGISTER_RESPONSES = {
    201: openapi.Response(
        description='Register user.',
        schema=openapi.Schema(
            title='User',
            type=openapi.TYPE_OBJECT,
            properties={
                'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING),
                'email': openapi.Schema(title='Email', type=openapi.TYPE_STRING, format=openapi.FORMAT_EMAIL),
                'username': openapi.Schema(title='Username', type=openapi.TYPE_STRING),
                'host': openapi.Schema(title='Host', type=openapi.TYPE_STRING, format=openapi.FORMAT_URI),
            }
        )
    )
}


REGISTER_ACTIVATION_RESPONSES = {
    200: openapi.Response(
        description='Activation successful.',
        schema=openapi.Schema(
            title='User',
            type=openapi.TYPE_OBJECT,
            properties={
                'response': openapi.Schema(title='Response', type=openapi.TYPE_STRING,
                                           description='User successfully activated.'),
            }
        )
    )
}


REVOKE_TOKEN_RESPONSES = {
    200: openapi.Response(
        description='Deleted',
    )
}

TOKEN_RESPONSES = {
    200: openapi.Response(
        description='Created',
        schema=openapi.Schema(
            title='Token',
            type=openapi.TYPE_OBJECT,
            properties={
                'access_token': openapi.Schema(title='Access token', type=openapi.TYPE_STRING),
                'expires_in': openapi.Schema(title='Expires in', type=openapi.TYPE_INTEGER),
                'token_type': openapi.Schema(title='Token type', type=openapi.TYPE_STRING),
                'scope': openapi.Schema(title='Scope', type=openapi.TYPE_STRING),
                'refresh_token': openapi.Schema(title='Refresh token', type=openapi.TYPE_STRING),
            }
        )
    )
}