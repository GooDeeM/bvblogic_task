from django.core.signing import BadSignature
from django.contrib.auth.mixins import LoginRequiredMixin

from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from rest_framework_social_oauth2.views import TokenView, RevokeTokenView
from oauth2_provider.models import Application

from . import response_schemas
from .serializers import RegisterSerializer, TokenSerializerSwagger, RevokeTokenSerializerSwagger
from api.utils import signer
from authorization.models import User

from drf_yasg.utils import swagger_auto_schema


class AuthTestView(APIView, LoginRequiredMixin):

    def get(self, request, *args, **kwargs):
        """
        sample API that returns True if user is authorized.
        """
        if request.user.is_authenticated:
            return Response({'response': 'authenticated'}, status=status.HTTP_200_OK)
        else:
            return Response({'response': 'not authenticated'}, status=status.HTTP_401_UNAUTHORIZED)


class RegistrationView(APIView):

    @swagger_auto_schema(request_body=RegisterSerializer, responses=response_schemas.REGISTER_RESPONSES)
    def post(self, request, *args, **kwargs):
        """
        Creating new user and send confirmation email.
        ---
        password and password2 must much.
        """
        serializer = RegisterSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()

            data['response'] = 'Registration successful.'
            data['email'] = account.email
            data['username'] = account.username
            data['host'] = serializer.validated_data['host']
            return Response(data, status=status.HTTP_201_CREATED)

        else:
            data = serializer.errors
            return Response(data, status=status.HTTP_400_BAD_REQUEST)


class RegisterActivationView(APIView):

    @swagger_auto_schema(responses=response_schemas.REGISTER_ACTIVATION_RESPONSES)
    def get(self, request, sign, *args, **kwargs):
        """
        user activation
        ---
        redirect to this link from email to change user status to activated.
        :param sign: activation sign from email
        :type sign: str
        """
        data = {}

        try:
            username = signer.unsign(sign)
        except BadSignature:
            data['response'] = 'bad signature'
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise NotFound(detail='User not found.')

        if user.is_active:
            data['response'] = 'User is already activated.'
            return Response(data, status=status.HTTP_409_CONFLICT)
        else:
            user.is_active = True
            user.save()
            data['response'] = 'User successfully activated.'
            return Response(data, status=status.HTTP_200_OK)


class SwaggerTokenView(TokenView):
    """
    Implements an endpoint to provide access tokens.
    ---
    to get client_id and client_secret you need to make /api/authorization/auth_apps/ request and this credentials will
    be returned on "base auth" app.
    """
    @swagger_auto_schema(request_body=TokenSerializerSwagger, responses=response_schemas.TOKEN_RESPONSES)
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class SwaggerRevokeTokenView(RevokeTokenView):
    """
    Implements an endpoint to revoke access or refresh tokens
    """
    @swagger_auto_schema(request_body=RevokeTokenSerializerSwagger, responses=response_schemas.REVOKE_TOKEN_RESPONSES)
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class OauthAppsList(APIView):

    @swagger_auto_schema(responses=response_schemas.AUTH_APPS_LIST_RESPONSES)
    def get(self, request, *args, **kwargs):
        """
        list of apps with its name, client_id and client_secret
        ---
        Use name parameter as backend on authorization via social networks.
        """
        apps = Application.objects.all()
        apps = [{'name_backend': app.name, 'client_id': app.client_id, 'client_secret': app.client_secret} for app in apps]

        return Response(apps, status=status.HTTP_200_OK) if apps else Response(status=status.HTTP_404_NOT_FOUND)