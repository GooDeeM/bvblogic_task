from rest_framework import serializers

from authorization.models import User

from api.utils import send_register_activation


class RegisterSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    host = serializers.URLField(required=True, help_text='host for activation link generation')

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password', 'password2', 'host']
        extra_kwargs = {
            'password': {'write_only': True},
            'email': {'required': True}
        }

    def save(self):
        user = User(
            email=self.validated_data['email'],
            username=self.validated_data['username'],
            is_active=False
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        # validating passwords
        if password != password2:
            raise serializers.ValidationError({'password': 'Passwords must match.'})
        user.set_password(password)

        # add first and last name if given
        if 'first_name' in self.validated_data.keys():
            user.first_name = self.validated_data['first_name']
        if 'last_name' in self.validated_data.keys():
            user.last_name = self.validated_data['last_name']

        # send activation email
        send_register_activation(self.validated_data['username'],
                                 self.validated_data['email'],
                                 self.validated_data['host']
                                 )

        user.save()

        return user


class TokenSerializerSwagger(serializers.Serializer):
    """
    only for swagger
    """
    grant_type = serializers.CharField(required=True)
    client_id = serializers.CharField(required=True)
    client_secret = serializers.CharField(required=True)
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True, write_only=True, style={'input_type': 'password'})

    class Meta:
        fields = ["grant_type", "client_id", "client_secret", "username", "password"]


class RevokeTokenSerializerSwagger(serializers.Serializer):
    """
    only for swagger
    """
    token = serializers.CharField(required=True)


    class Meta:
        fields = ["grant_type", "client_id", "client_secret", "username", "password"]
