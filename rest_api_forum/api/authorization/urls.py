from django.urls import path

from .views import (
    RegistrationView, RegisterActivationView, SwaggerTokenView, SwaggerRevokeTokenView, OauthAppsList, AuthTestView
)

urlpatterns = [
    path('test/', AuthTestView.as_view(), name='authorization_test'),
    path('register/activation/<str:sign>/', RegisterActivationView.as_view(), name='register activation'),
    path('register/', RegistrationView.as_view(), name='register'),
    path('token/', SwaggerTokenView.as_view(), name="token"),
    path('revoke-token/', SwaggerRevokeTokenView.as_view(), name="revoke_token"),
    path('auth_apps/', OauthAppsList.as_view(), name='auth_apps_list'),
]
