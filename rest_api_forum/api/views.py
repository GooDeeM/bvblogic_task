from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_social_oauth2.views import ConvertTokenView


class TestView(APIView):
    def get(self, request, *args, **kwargs):
        return Response('Hello world')


